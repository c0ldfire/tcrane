workspace "TowerCrane"
    configurations { "Debug", "Release" }
    startproject "CraneGUI"
    location "build"
    targetdir "bin"

    filter "configurations:Debug"
        defines "DEBUG"
        flags { "Symbols" }

    filter "configurations:Release"
        defines "NDEBUG"
        optimize "On"


    project "CraneGUI"
        kind "WindowedApp"
        language "C#"
        framework "4.0"
        architecture "x86"
        namespace "TowerCrane.GUI"
        location "build"

        files {
            "source/GUI/**"
        }

        links {
            "System",
            "System.Drawing",
            "System.Windows.Forms",
            "System.Windows.Forms.DataVisualization",
			"OpenTK",
			"OpenTK.GLControl",
            "CraneController"
        }


    project "CraneController"
        kind "SharedLib"
        language "C#"
        framework "4.0"
        architecture "x86"
        namespace "TowerCrane"
        location "build"
        dependson "RtdacUsbDriver"

        files {
            "source/Driver/*.cs",
            "source/Joystick/*.cs",
            "source/*.cs",
        }

        links {
            "System",
            "System.Xml",
            "OpenTK"
        }


    project "RtdacUsbDriver"
        kind "SharedLib"
        language "C++"
        architecture "x86"
        location "build"
        includedirs { "3rdparty/ftd2xx/include" }
        libdirs { "3rdparty/ftd2xx/lib" }
		defines "EXPORT_SYMBOLS"

        files {
            "source/Driver/*.h",
            "source/Driver/*.c"
        }
        
        links {
            "ftd2xx"
        }
