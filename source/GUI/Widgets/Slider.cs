using System;
using System.Windows.Forms;


namespace TowerCrane.GUI.Widgets
{
    public class Slider : UserControl
    {
        protected TableLayoutPanel table = new TableLayoutPanel();
        protected NumericUpDown spinbox = new NumericUpDown();
        protected TrackBar trackbar = new TrackBar();
        protected Label label = new Label();

        #region Public events
        public event EventHandler Changed;
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public override string Text {
            get { return label.Text; }
            set { label.Text = value; }
        }

        /// <summary>
        /// Gets or sets the minimum value for the slider.
        /// </summary>
        public decimal Minimum {
            get { return spinbox.Minimum; }
            set {
                spinbox.Minimum = value;
                UpdateTrackBarRange();
            }
        }

        /// <summary>
        /// Gets or sets the maximum value for the slider.
        /// </summary>
        public decimal Maximum {
            get { return spinbox.Maximum; }
            set {
                spinbox.Maximum = value;
                UpdateTrackBarRange();
            }
        }

        /// <summary>
        /// Gets or sets the value assigned to the slider.
        /// </summary>
        public decimal Value {
            get { return spinbox.Value; }
            set {
                spinbox.Value = value;
                trackbar.Value = (int)(value / spinbox.Increment);
                if (Changed != null) Changed.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets slider's resolution (step size).
        /// </summary>
        public decimal Increment {
            get { return spinbox.Increment; }
            set {
                spinbox.Increment = value;
                UpdateTrackBarRange();
            }
        }

        /// <summary>
        /// Gets or sets the number of decimal places to display in the spin box.
        /// </summary>
        public int DecimalPlaces
        {
            get { return spinbox.DecimalPlaces; }
            set { spinbox.DecimalPlaces = value; }
        }
        #endregion


        public Slider()
        {
            SuspendLayout();
                label.Anchor = AnchorStyles.Left;
                spinbox.Anchor = AnchorStyles.Right;
                trackbar.TickStyle = TickStyle.None;
                trackbar.Dock = DockStyle.Fill;

                table.ColumnCount = table.RowCount = 2;
                table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 60));
                table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
                table.Dock = DockStyle.Fill;
                table.Controls.Add(label, 0, 0);
                table.Controls.Add(spinbox, 1, 0);
                table.Controls.Add(trackbar, 0, 1);
                table.SetColumnSpan(trackbar, 2);
                table.PerformLayout();

                Controls.Add(table);
            ResumeLayout(false);

            trackbar.ValueChanged += (sender, e) => spinbox.Value = (decimal)trackbar.Value * Increment;
            spinbox.ValueChanged += (sender, e) => trackbar.Value = (int)(spinbox.Value / Increment);
            trackbar.ValueChanged += (sender, e) => Changed.Invoke(this, e);
            spinbox.ValueChanged += (sender, e) => Changed.Invoke(this, e);
        }


        protected void UpdateTrackBarRange()
        {
            trackbar.Minimum = (int)(spinbox.Minimum / spinbox.Increment);
            trackbar.Maximum = (int)(spinbox.Maximum / spinbox.Increment);
        }
    }
}