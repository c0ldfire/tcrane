﻿using System;
using System.Windows.Forms;

namespace TowerCrane.GUI.source.GUI.Widgets
{
    public partial class ServiceAuth : UserControl
    {
        protected TableLayoutPanel table = new TableLayoutPanel();
        protected Label label = new Label();
        protected TextBox box = new TextBox();
        protected Button button = new Button();

        #region Public events
        public event EventHandler Auth;
        #endregion

        public ServiceAuth(string password)
        {
            Password = password;

            SuspendLayout();

            label.Text = "Service password";
            box.Dock = DockStyle.Fill;
            box.PasswordChar = '*';
            button.Text = "Log in";

            table.Dock = DockStyle.Fill;
            table.ColumnCount = 2;
            table.RowCount = 2;

            table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            table.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 84.0f));
            table.RowStyles.Add(new RowStyle(SizeType.Absolute, 15.0f));
            table.RowStyles.Add(new RowStyle(SizeType.Absolute, 15.0f));

            table.Controls.Add(label, 0, 0);
            table.Controls.Add(box, 0, 1);
            table.Controls.Add(button, 1, 1);

            Controls.Add(table);
            ResumeLayout(false);
            table.PerformLayout();

            box.KeyDown += (sender, e) =>
            {
                if (e.KeyCode == Keys.Enter)
                {
                    button.PerformClick();
                    e.SuppressKeyPress = true;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    Reset();
                    e.SuppressKeyPress = true;
                }
            };

            button.Click += (sender, e) => 
            {
                if (box.Text == password)
                    IsAuth = !IsAuth;
                else
                    IsAuth = false;
                box.Text = string.Empty;
                Auth.Invoke(this, new EventArgs());
            };

            this.Auth += (sender, e) =>
            {
                if (IsAuth)
                    button.Text = "Log out";
                else
                    button.Text = "Log in";
            };
        }

        public void Reset()
        {
            IsAuth = false;
            box.Text = string.Empty;
            button.Text = "Log in";
        }

        public bool IsAuth
        {
            get;
            private set;
        }

        public string Password
        {
            get;
            private set;
        }
    }
}
