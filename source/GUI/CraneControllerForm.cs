﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TowerCrane.GUI.Views;


namespace TowerCrane.GUI
{
    public class CraneControllerForm : Form
    {
    	#region Non-public class fields
    	protected SettingsForm settings;
    	protected SplitContainer sidesplit = new SplitContainer();
    	protected TabControl viewTabs = new TabControl();
    	protected ToolStripMenuItem fileMenu = new ToolStripMenuItem("&File");
    	protected ToolStripMenuItem viewMenu = new ToolStripMenuItem("&View");
    	protected ToolStripMenuItem toolMenu = new ToolStripMenuItem("&Tools");
    	protected ToolStripMenuItem helpMenu = new ToolStripMenuItem("&Help");
    	protected ToolStripMenuItem visualizationEntry;
    	protected ToolStripItem[] fileSubmenu;
    	protected ToolStripItem[] toolSubmenu;
    	protected ToolStripItem[] helpSubmenu;
    	protected CraneController crane;
    	protected CraneVisualizationView canvas;
    	#endregion

        public CraneControllerForm()
        {
        	SetupUI();
        	
        	// Try to load previously saved configuration
        	Configuration config = Configuration.Load("custom.conf");
        	settings = new SettingsForm(config);

            // Subscribe menu click handlers
            visualizationEntry.Click += (sender,e) => sidesplit.Panel1Collapsed = !visualizationEntry.Checked;
            toolSubmenu[0].Click += (sender, e) => crane.Stop();
            toolSubmenu[4].Click += (sender, e) => settings.ShowDialog();
            fileSubmenu[5].Click += (sender, e) => Application.Exit();

            // Initialize crane controller and spawn some views
            var ptc = new PhysicalTowerCrane(config);
            crane = new CraneController(ptc);
            RegisterView(new ManualSteeringTabPage(crane));
            RegisterView(new ChartView(crane));
            RegisterView(new GameModeView(crane));
            RegisterView(new DebugView(crane), false);
            RegisterView(new SettingsView(crane, config));
            
            // Create crane visualization view
            canvas = new CraneVisualizationView(crane);
            canvas.Dock = DockStyle.Fill;
        	sidesplit.Panel1.Controls.Add(canvas);

            crane.Crane.Updated += (sender, e) => canvas.Invalidate(false);
        }


        public void RegisterView(CraneViewTabPage page, bool visible=true)
        {
            if (visible)
                viewTabs.TabPages.Add(page);

            // Create a menu item and append it to View submenu.
            var menuItem = new ToolStripMenuItem();
            menuItem.CheckOnClick = true;
            menuItem.Checked = visible;
            menuItem.Text = page.Text;
            menuItem.Tag = page;
            viewMenu.DropDownItems.Add(menuItem);
            page.Tag = menuItem;

            // Handle mouse clicks
            menuItem.Click += (sender, e) => {
                var tab = (CraneViewTabPage)menuItem.Tag;
                if (!menuItem.Checked)
                    viewTabs.TabPages.Remove(tab);
                else
                    viewTabs.TabPages.Add(tab);
            };
        }
        
        
        protected void SetupUI()
        {
        	fileSubmenu = new ToolStripItem[] {
        		new ToolStripMenuItem("&New"),
        		new ToolStripMenuItem("&Open"),
        		new ToolStripMenuItem("&Save"),
        		new ToolStripMenuItem("Save &as"),
        		new ToolStripSeparator(),
        		new ToolStripMenuItem("E&xit")
        	};
        	visualizationEntry = new ToolStripMenuItem("3D &visualization");
        	visualizationEntry.CheckOnClick = true;
        	visualizationEntry.Checked = true;
        	toolSubmenu = new ToolStripItem[] {
        		new ToolStripMenuItem("&Stop"),
        		new ToolStripMenuItem("Go &home"),
        		new ToolStripMenuItem("Go &center"),
        		new ToolStripSeparator(),
        		new ToolStripMenuItem("Se&ttings")
        	};
        	helpSubmenu = new ToolStripItem[] {
        		new ToolStripMenuItem("&About")
        	};
        	fileMenu.DropDownItems.AddRange(fileSubmenu);
        	viewMenu.DropDownItems.Add(visualizationEntry);
        	toolMenu.DropDownItems.AddRange(toolSubmenu);
        	helpMenu.DropDownItems.AddRange(helpSubmenu);
        	
        	var mainMenu = new MenuStrip();
        	mainMenu.Items.AddRange(new ToolStripItem[] {
        		fileMenu, viewMenu,	toolMenu, helpMenu
        	});
        	
        	viewTabs.Dock = DockStyle.Fill;
        	viewTabs.Alignment = TabAlignment.Bottom;
        	sidesplit.Dock = DockStyle.Fill;
        	sidesplit.Panel2.Controls.Add(viewTabs);
        	Controls.Add(sidesplit);
        	Controls.Add(mainMenu);
        	MainMenuStrip = mainMenu;
        	ClientSize = new Size(600, 400);
        	Text = "Crane Controller";
        }
    }
}
