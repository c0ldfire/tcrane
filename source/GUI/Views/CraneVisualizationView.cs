﻿using System;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace TowerCrane.GUI.Views
{
	public class CraneVisualizationView : GLControl
    {
		#region Non-public class fields
		protected CraneController controller;
		#endregion
		
		public CraneVisualizationView(CraneController craneController)
		{
			controller = craneController;
            Load += (sender, e) => {
				Initialize();
            	Paint += OnRenderFrame;
            	Invalidate();
            };
		}
		
		
		protected bool Initialize()
		{
			controller.Crane.Updated += (sender, evt) => Invalidate();
			controller.SteeringChanged += (sender, evt) => Invalidate();
			// TODO: Create/load resources needed by visualization
			return true;
		}
		
        protected void OnRenderFrame(object Sender, PaintEventArgs evt)
        {
        	GL.ClearColor(controller.CartEngine + 1.0f / 2.0f,
        	              controller.ArmEngine + 1.0f / 2.0f,
        	              controller.LiftEngine + 1.0f / 2.0f,
        	              1.0f);
        	GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        	// TODO: Implement crane visualization
        	SwapBuffers();
        }
    }
}
