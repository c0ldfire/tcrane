﻿using System;
using System.Windows.Forms;
using TowerCrane;
using TowerCrane.GUI.Widgets;


namespace TowerCrane.GUI.Views
{
    public class ManualSteeringTabPage : CraneViewTabPage
    {
    	#region Non-public class fields
    	protected TableLayoutPanel table = new TableLayoutPanel();
    	protected Slider xslider = new Slider();
    	protected Slider yslider = new Slider();
    	protected Slider zslider = new Slider();
        protected bool suppressUpdate = false;
        #endregion
        
        public ManualSteeringTabPage(CraneController crane)
            : base(crane)
        {
        	Text = "Manual steering";
        	SetupUI();
        	
        	xslider.Changed += (sender, e) => 
            {
                suppressUpdate = true;
                crane.CartEngine = (float)xslider.Value;
                suppressUpdate = false;
            };
            yslider.Changed += (sender, e) =>
            {
                suppressUpdate = true;
                crane.ArmEngine = (float)yslider.Value;
                suppressUpdate = false;
            };
            zslider.Changed += (sender, e) =>
            {
                suppressUpdate = true;
                crane.LiftEngine = (float)zslider.Value;
                suppressUpdate = false;
            };
            crane.SteeringChanged += (sender, e) => 
            {
                if (!suppressUpdate) {
                    xslider.Value = (decimal)crane.CartEngine;
                    yslider.Value = (decimal)crane.ArmEngine;
                    zslider.Value = (decimal)crane.LiftEngine;
                }
            };
        }

        protected void SetupUI()
        {
        	table.Dock = DockStyle.Fill;
        	table.ColumnCount = 1;
        	table.RowCount = 3;
        	table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
        	table.RowStyles.Add(new RowStyle(SizeType.Percent, 1.0f/3.0f));
        	table.RowStyles.Add(new RowStyle(SizeType.Percent, 1.0f/3.0f));
        	table.RowStyles.Add(new RowStyle(SizeType.Percent, 1.0f/3.0f));
        	
        	const decimal STEP_SIZE = (decimal)1.0 / (decimal)1024;
        	xslider.Dock = DockStyle.Fill;
        	xslider.DecimalPlaces = 3;
        	xslider.Increment = STEP_SIZE;
        	xslider.Maximum = (decimal)1.0;
        	xslider.Minimum = (decimal)-1.0;
        	xslider.Text = "X axis (cart)";
        	
        	yslider.Dock = DockStyle.Fill;
        	yslider.DecimalPlaces = 3;
        	yslider.Increment = STEP_SIZE;
        	yslider.Maximum = (decimal)1.0;
        	yslider.Minimum = (decimal)-1.0;
        	yslider.Text = "Y angle (arm)";
        	
        	zslider.Dock = DockStyle.Fill;
        	zslider.DecimalPlaces = 3;
        	zslider.Increment = STEP_SIZE;
        	zslider.Maximum = (decimal)1.0;
        	zslider.Minimum = (decimal)-1.0;
        	zslider.Text = "Z axis (line)";
        	
        	table.Controls.Add(xslider, 0, 0);
        	table.Controls.Add(yslider, 0, 1);
        	table.Controls.Add(zslider, 0, 2);
        	
        	Controls.Add(table);
        }
    }
}
