﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace TowerCrane.GUI.Views
{
    public class DebugView : CraneViewTabPage
    {
    	#region Non-public class fields
    	protected TableLayoutPanel table = new TableLayoutPanel();
    	protected Dictionary<string, GroupBox> groups = new Dictionary<string, GroupBox>();
    	protected TextBox[] pwmBoxes;
    	protected TextBox[] limitBoxes;
    	protected TextBox[] encoderBoxes;
    	#endregion
    	
    	
        public DebugView(CraneController crane)
            : base(crane)
        {
        	Text = "Debug mode";
        	SetupUI();
        }

        
        protected void SetupUI()
        {
        	// Create group boxes filled with read-only fields
        	var AXES = new string[] { "X", "Y", "Z" };
        	var ENCODERS = new string[] { "X", "Y", "Z", "A", "B" };
        	var pwmGroup = SetupGroup("PWM Generators", AXES, out pwmBoxes);
        	var limitGroup = SetupGroup("Rail limits", AXES, out limitBoxes);
        	var encoderGroup = SetupGroup("Encoders", ENCODERS, out encoderBoxes);
	        
        	// Setup table for laying out groups created above
        	table.Dock = DockStyle.Fill;
        	table.ColumnCount = table.RowCount = 2;
        	for (int i=0; i < table.ColumnCount; ++i)
        		table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f / table.ColumnCount));
        	for (int i=0; i < table.RowCount; ++i)
        		table.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f / table.ColumnCount));
        	table.SuspendLayout();
	        	table.Controls.Add(pwmGroup, 0, 0);
	        	table.Controls.Add(limitGroup, 0, 1);
	        	table.Controls.Add(encoderGroup, 1, 0);
	        	table.SetRowSpan(encoderGroup, 2);
        	table.ResumeLayout();
        	Controls.Add(table);
        	
        	PhysicalTowerCrane ptc = crane.Crane;
            EventHandler handler = (sender, e) => {
            	var state = ptc.InternalState;
            	for (int i=0; i < 3; ++i) {
            		pwmBoxes[i].Text = state.PwmGenerators[i].Width.ToString();
            		limitBoxes[i].Text = state.Limits[i].Value.ToString();
            	}
            	for (int i=0; i < 5; ++i)
            		encoderBoxes[i].Text = state.Encoders[0].Value.ToString();
            };
            ptc.Updated += handler;
            ptc.SteeringChanged += handler;
        }
        
        protected GroupBox SetupGroup(string title, string[] labels, out TextBox[] boxes)
        {
        	boxes = new TextBox[labels.Length];
        	
        	// Create table for laying out the fields
        	var panel = new TableLayoutPanel();
        	panel.Dock = DockStyle.Fill;
        	panel.ColumnCount = 2;
        	panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20.0f));
        	panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80.0f));
        	panel.RowCount = labels.Length;
        	float HEIGHT = 100.0f / labels.Length;
        	for (int i=0; i < labels.Length; ++i)
        		panel.RowStyles.Add(new RowStyle(SizeType.Percent, HEIGHT));
        	
        	// Fill rows
        	for (int i=0; i < labels.Length; ++i) {
        		var label = new Label();
        		label.AutoSize = true;
        		label.Anchor = AnchorStyles.Right;
        		label.Text = labels[i];
        		panel.Controls.Add(label, 0, i);
        		
        		boxes[i] = new TextBox();
        		boxes[i].Anchor = AnchorStyles.Left | AnchorStyles.Right;
        		boxes[i].ReadOnly = true;
        		panel.Controls.Add(boxes[i], 1, i);
        	}
        	
        	// Create group box
        	var group = new GroupBox();
        	group.Dock = DockStyle.Fill;
        	group.Text = title;
        	group.Controls.Add(panel);
        	group.Tag = panel;
        	
        	groups[title] = group;
        	return group;
        }
    }
}
