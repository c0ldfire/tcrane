﻿using System;
using System.Windows.Forms;


namespace TowerCrane.GUI.Views
{
    public class GameModeView : CraneViewTabPage
    {
        public GameModeView(CraneController crane)
            : base(crane)
        {
            Text = "Game Mode";
            SetupUI();
        }


        protected void SetupUI()
        {
            var label = new Label();
            label.AutoSize = true;
            label.Text = "Ta strona będzie zawierała tryb \"gry\" mający na celu zmotywować użytkowników...";
            Controls.Add(label);
        }
    }
}
