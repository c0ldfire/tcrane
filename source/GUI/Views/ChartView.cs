﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace TowerCrane.GUI.Views
{
    public class ChartView : CraneViewTabPage
    {
    	#region Non-public class fields
    	protected TableLayoutPanel grid = new TableLayoutPanel();
    	protected Series xseries = new Series();
    	protected Series yseries = new Series();
    	protected Series zseries = new Series();
    	protected Series payloadSeries = new Series();
    	protected Chart xchart = new Chart();
    	protected Chart ychart = new Chart();
    	protected Chart zchart = new Chart();
    	protected Chart payloadChart = new Chart();
    	#endregion
    	
        public ChartView(CraneController crane)
            : base(crane)
        {
        	SetupUI();
        	Text = "Charts";
        	
        	// Plot some random charts
        	for (int i=1; i < 20; ++i) {
                xchart.Series[0].Points.AddXY(i, i*i);
                ychart.Series[0].Points.AddXY(i * i, i / 2);
                zchart.Series[0].Points.AddXY(i, Math.Sqrt(i));
            }
            for (int i=1; i < 600; ++i)
            	// mani3xis' fractal
                payloadChart.Series[0].Points.AddXY(
                        10 * Math.Sin(i) * Math.Cos(i * 36.0 * Math.PI / 180.0),
                        10 * Math.Sin(i) * Math.Sin(i * 36.0 * Math.PI / 180.0)
                    );
        }

        protected void SetupUI()
        {
        	xseries.ChartType = SeriesChartType.Line;
        	xchart.Dock = DockStyle.Fill;
        	xchart.Series.Add(xseries);
        	xchart.ChartAreas.Add(new ChartArea());
        	
        	yseries.ChartType = SeriesChartType.Line;
        	ychart.Dock = DockStyle.Fill;
        	ychart.Series.Add(yseries);
        	ychart.ChartAreas.Add(new ChartArea());
        	
        	zseries.ChartType = SeriesChartType.Line;
        	zchart.Dock = DockStyle.Fill;
        	zchart.Series.Add(zseries);
        	zchart.ChartAreas.Add(new ChartArea());
        	
        	payloadSeries.ChartType = SeriesChartType.Line;
        	payloadChart.Dock = DockStyle.Fill;
        	payloadChart.Series.Add(payloadSeries);
        	payloadChart.ChartAreas.Add(new ChartArea());
        	
        	grid.Dock = DockStyle.Fill;
        	grid.RowCount = grid.ColumnCount = 2;
        	grid.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
        	grid.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
        	grid.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
        	grid.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
        	grid.Controls.Add(xchart, 0, 0);
        	grid.Controls.Add(ychart, 0, 1);
        	grid.Controls.Add(zchart, 1, 0);
        	grid.Controls.Add(payloadChart, 1, 1);
        	Controls.Add(grid);
        }
    }
}
