﻿using System;
using System.Windows.Forms;
using TowerCrane.GUI.source.GUI.Widgets;

namespace TowerCrane.GUI.Views
{
    public class SettingsView : CraneViewTabPage
    {
        #region Non-public class fields
        protected Configuration config;
        protected TableLayoutPanel table = new TableLayoutPanel();
        protected TableLayoutPanel sldTable = new TableLayoutPanel();
        protected ServiceAuth auth = new ServiceAuth("magiczne");
        protected NumericUpDown[] limits = new NumericUpDown[3];
        protected CheckBox[] limitFlags = new CheckBox[3];
        protected CheckBox[] thermFlags = new CheckBox[3];
        protected NumericUpDown[] callib = new NumericUpDown[5];
        protected Label[] callibLabel = new Label[5];
        protected CheckBox autoReset = new CheckBox();
        protected Button saveButton = new Button();
        protected Button loadButton = new Button();
        protected Button loadDefButton = new Button();
        #endregion

        public SettingsView(CraneController crane, Configuration config) : base(crane)
        {
            this.config = config;
            Text = "Settings";
            SetupUI();

            auth.Auth += (sender, e) => SetAllEnable(auth.IsAuth);

            limitFlags[0].CheckedChanged += (sender, e) => config.RailLimits.XFlag = limitFlags[0].Checked;
            limitFlags[1].CheckedChanged += (sender, e) => config.RailLimits.YFlag = limitFlags[1].Checked;
            limitFlags[2].CheckedChanged += (sender, e) => config.RailLimits.ZFlag = limitFlags[2].Checked;

            limits[0].ValueChanged += (sender, e) => config.RailLimits.XScale = (float)limits[0].Value;
            limits[1].ValueChanged += (sender, e) => config.RailLimits.YScale = (float)limits[1].Value;
            limits[2].ValueChanged += (sender, e) => config.RailLimits.ZScale = (float)limits[2].Value;

            thermFlags[0].CheckedChanged += (sender, e) => config.ThermFlags.XFlag = thermFlags[0].Checked;
            thermFlags[1].CheckedChanged += (sender, e) => config.ThermFlags.YFlag = thermFlags[1].Checked;
            thermFlags[2].CheckedChanged += (sender, e) => config.ThermFlags.ZFlag = thermFlags[2].Checked;

            callib[0].ValueChanged += (sender, e) => config.Callibration.XScale = (float)callib[0].Value;
            callib[1].ValueChanged += (sender, e) => config.Callibration.YScale = (float)callib[1].Value;
            callib[2].ValueChanged += (sender, e) => config.Callibration.ZScale = (float)callib[2].Value;
            callib[3].ValueChanged += (sender, e) => config.Callibration.AlphaScale = (float)callib[3].Value;
            callib[4].ValueChanged += (sender, e) => config.Callibration.BetaScale = (float)callib[4].Value;

            autoReset.CheckedChanged += (sender, e) => config.AutoReset = autoReset.Checked;

            saveButton.Click += (sender, e) => config.Save("custom.conf");
            loadButton.Click += (sender, e) => 
            {
                this.config = Configuration.Load("custom.conf");
                UpdateControls();
            };
            loadDefButton.Click += (sender, e) => 
            {
                this.config = new Configuration();
                UpdateControls();
            };
        }

        protected void SetupUI()
        {
            //table.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;
            //sldTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Inset;

            table.Dock = DockStyle.Fill;
            table.ColumnCount = 3;
            table.RowCount = 8;
            for (int i = 0; i < table.ColumnCount; i++)
                table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f / table.ColumnCount));
            table.RowStyles.Add(new RowStyle(SizeType.Absolute, 50.0f));
            for (int i = 1; i < table.RowCount; i++)
                table.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f / table.RowCount / 2));

            auth.Dock = DockStyle.Fill;
            table.SetColumnSpan(auth, 3);
            table.Controls.Add(auth, 0, 0);

            for (int i = 0; i < 3; i++)
            {
                limitFlags[i] = new CheckBox();
                InitFlag(limitFlags[i], (char)('X' + i) + " axis limit", AnchorStyles.Left | AnchorStyles.Bottom);
                table.Controls.Add(limitFlags[i], i, 1);

                limits[i] = new NumericUpDown();
                limits[i].Anchor = AnchorStyles.Left | AnchorStyles.Top;
                limits[i].DecimalPlaces = 6;
                limits[i].Increment = (decimal)0.001;
                table.Controls.Add(limits[i], i, 2);

                thermFlags[i] = new CheckBox();
                InitFlag(thermFlags[i], (char)('X' + i) + " axis thermal", AnchorStyles.Left);
                table.Controls.Add(thermFlags[i], i, 3);
            }

            for (int i = 0; i < 5; i++)
            {
                callibLabel[i] = new Label();
                callibLabel[i].Enabled = false;
                callibLabel[i].Padding = new Padding(0, 8, 0, 0);
                callibLabel[i].Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
                callibLabel[i].Text = (char)('X' + i - Convert.ToInt32(i > 2) * 26) + " scale";
                table.Controls.Add(callibLabel[i], i % 3, 4 + Convert.ToInt32(i > 2) * 2);

                callib[i] = new NumericUpDown();
                callib[i].Enabled = false;
                callib[i].Anchor = AnchorStyles.Left | AnchorStyles.Top;
                callib[i].DecimalPlaces = 6;
                callib[i].Increment = (decimal)0.001;
                table.Controls.Add(callib[i], i % 3, 5 + Convert.ToInt32(i > 2) * 2);
            }

            InitFlag(autoReset, "Auto reset", AnchorStyles.Bottom);
            table.Controls.Add(autoReset, 2, 6);

            InitSldButton(saveButton, "Save");
            InitSldButton(loadButton, "Load");
            InitSldButton(loadDefButton, "Default");

            sldTable.RowCount = 1;
            sldTable.ColumnCount = 3;
            for (int i = 0; i < sldTable.ColumnCount; i++)
                sldTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f / sldTable.ColumnCount));
            sldTable.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            sldTable.Controls.Add(saveButton, 0, 0);
            sldTable.Controls.Add(loadButton, 1, 0);
            sldTable.Controls.Add(loadDefButton, 2, 0);
            sldTable.Margin = new Padding(0);
            sldTable.Anchor = AnchorStyles.Top;
            table.Controls.Add(sldTable, 2, 7);

            SetAllEnable(false);
            UpdateControls();

            Controls.Add(table);
        }

        private void SetAllEnable(bool state)
        {
            foreach (Control c in limits)
                c.Enabled = state;
            foreach (Control c in limitFlags)
                c.Enabled = state;
            foreach (Control c in thermFlags)
                c.Enabled = state;
            foreach (Control c in callib)
                c.Enabled = state;
            foreach (Control c in callibLabel)
                c.Enabled = state;

            autoReset.Enabled = state;
            saveButton.Enabled = state;
            loadButton.Enabled = state;
            loadDefButton.Enabled = state;
        }

        private void InitFlag(CheckBox checkbox, string text, AnchorStyles anchor)
        {
            checkbox.AutoSize = true;
            checkbox.Text = text;
            checkbox.Anchor = anchor;
            checkbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        }

        private void InitSldButton(Button button, string text)
        {
            button.Text = text;
            button.Margin = new Padding(0);
            button.Anchor = AnchorStyles.Top;
        }

        private void UpdateControls()
        {
            limitFlags[0].Checked = config.RailLimits.XFlag;
            limitFlags[1].Checked = config.RailLimits.YFlag;
            limitFlags[2].Checked = config.RailLimits.ZFlag;

            limits[0].Value = (decimal)config.RailLimits.XScale;
            limits[1].Value = (decimal)config.RailLimits.YScale;
            limits[2].Value = (decimal)config.RailLimits.ZScale;

            thermFlags[0].Checked = config.ThermFlags.XFlag;
            thermFlags[1].Checked = config.ThermFlags.YFlag;
            thermFlags[2].Checked = config.ThermFlags.ZFlag;

            callib[0].Value = (decimal)config.Callibration.XScale;
            callib[1].Value = (decimal)config.Callibration.YScale;
            callib[2].Value = (decimal)config.Callibration.ZScale;
            callib[3].Value = (decimal)config.Callibration.AlphaScale;
            callib[4].Value = (decimal)config.Callibration.BetaScale;

            autoReset.Checked = config.AutoReset;
        }
    }
}
