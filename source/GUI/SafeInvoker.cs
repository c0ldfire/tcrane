﻿using System;
using System.Windows.Forms;


namespace TowerCrane.GUI
{
    public static class SafeInvoker
    {
        public static TResult SafeInvoke<TControl, TResult>(this TControl self, Func<TControl, TResult> func)
            where TControl : Control
        {
            if (self.InvokeRequired)
                return (TResult)self.Invoke(func, self);
            else
                return func(self);
        }
    }
}
