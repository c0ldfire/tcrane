﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace TowerCrane.GUI
{
    public class SettingsForm : Form
    {
    	protected TabControl categories = new TabControl();
    	protected Configuration config;
    	
        public SettingsForm(Configuration configuration)
        {
        	config = configuration;
            SetupUI();
        }
        
        protected void SetupUI()
        {
        	categories.Dock = DockStyle.Fill;
        	
        	ClientSize = new Size(400, 300);
        	Text = "Settings";
        	FormBorderStyle = FormBorderStyle.FixedDialog;
        	MaximizeBox = MinimizeBox = false;
        	Controls.Add(categories);
        }
    }
}
