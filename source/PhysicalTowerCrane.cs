﻿using System;
using TowerCrane.Driver;


namespace TowerCrane
{
	public class PhysicalTowerCrane
	{
        #region Non-public class fields
        protected const float EPSILON = 1e-5f;
        protected RtdacState state = new RtdacState();
        protected EncoderCallibration callibration;
        protected Configuration config;
        protected float xSteering;
        protected float ySteering;
        protected float zSteering;
        protected float xRailLimit;
        protected float yRailLimit;
        protected float zRailLimit;
        protected float cartOffset;
        protected float armAngle;
        protected float lineLength;
        protected float alphaAngle;
        protected float betaAngle;
        #endregion
        
        #region Publicly subscribable events
        public event EventHandler Updated;
        public event EventHandler SteeringChanged;
        public event EventHandler RailLimitChanged;
        #endregion
        
        #region Public properties (getters/setters)
            #region Engine steering properties
            /// <summary>
            /// Gets or sets the normalized value that controls the cart engine speed (X axis).
            /// </summary>
            public float XSteering {
                get { return xSteering; }
                set {
                    xSteering = value;
                    SteeringChanged.Raise(this);
                }
            }

            /// <summary>
            /// Gets or sets the normalized value that controls the arm rotation engine (Y axis).
            /// </summary>
            public float YSteering {
                get { return ySteering; }
                set {
                    ySteering = value;
                    SteeringChanged.Raise(this);
                }
            }

            /// <summary>
            /// Gets or sets the normalized value that controls the lift engine (Z axis).
            /// </summary>
            public float ZSteering {
                get { return zSteering; }
                set {
                    zSteering = value;
                    SteeringChanged.Raise(this);
                }
            }
            #endregion

            #region Callibrated values from encoders (read-only)
            /// <summary>
            /// Gets the cart distance (in meters) along the arm (X axis).
            /// </summary>
            public float XDistance {
                get { return cartOffset; }
            }

            /// <summary>
            /// Gets the arm angle (in radians).
            /// </summary>
            public float YAngle {
                get { return armAngle; }
            }

            /// <summary>
            /// Gets the length (in meters) of the lift-line (Z axis).
            /// </summary>
            public float ZLength {
                get { return lineLength; }
            }

            /// <summary>
            /// Gets the payload's vertical deviation angle (in radians).
            /// </summary>
            public float AlphaAngle {
                get { return alphaAngle; }
            }

            /// <summary>
            /// Gets the payload's horizontal deviation angle (in radians).
            /// </summary>
            public float BetaAngle {
                get { return betaAngle; }
            }
            #endregion

            #region Rail limits
            /// <summary>
            /// Gets or sets the maximum allowed cart offset (in meters).
            /// </summary>
            public float XRailLimit {
                get { return xRailLimit; }
                set {
                    xRailLimit = value;
                    RailLimitChanged.Raise(this);
                }
            }

            /// <summary>
            /// Gets or sets the maximum allowed arm angle (in radians).
            /// </summary>
            public float YRailLimit {
                get { return yRailLimit; }
                set {
                    yRailLimit = value;
                    RailLimitChanged.Raise(this);
                }
            }

            /// <summary>
            /// Gets or sets the maximum lift-line length (in meters).
            /// </summary>
            public float ZRailLimit {
                get { return zRailLimit; }
                set {
                    zRailLimit = value;
                    RailLimitChanged.Raise(this);
                }
            }
            #endregion
            
            public RtdacState InternalState {
            	get { return state; }
            }
        #endregion
        
		public PhysicalTowerCrane(Configuration config)
		{
            this.config = config;
			callibration = config.Callibration;
			
			if (!Initialize())
				throw new Exception("Crane initialization failed");
			SteeringChanged += (sender, e) => ApplyEngineSteering(xSteering, ySteering, zSteering);
			RailLimitChanged += (sender, e) => ApplyLimits(xRailLimit, yRailLimit, zRailLimit, true, true, true); // TODO: Remove hardcoded autostop flag
		}
		
		
		/// <summary>
		/// Initialize crane's hardware.
		/// </summary>
		public bool Initialize()
		{
			if (RtdacUsbDriver.Open() != 0)
				throw new Exception("Cannot open RTDAC/USB device");
			return true;
		}
		
		/// <summary>
        /// Update internal state by downloading the state from device.
        /// </summary>
        public void Update()
        {
			if (RtdacUsbDriver.Read(state) != 0)
            	throw new Exception("Failed to read data from RTDAC/USB device");

            cartOffset = state.Encoders[0].Value * callibration.XScale;
            armAngle   = state.Encoders[1].Value * callibration.YScale;
            lineLength = state.Encoders[2].Value * callibration.ZScale;
            alphaAngle = state.Encoders[3].Value * callibration.AlphaScale;
            betaAngle  = state.Encoders[4].Value * callibration.BetaScale;
            xRailLimit = state.Limits[0].Value * 64.0f * callibration.XScale;
            yRailLimit = state.Limits[1].Value * 64.0f * callibration.YScale;
            zRailLimit = state.Limits[2].Value * 64.0f * callibration.ZScale;
            
            Updated.Raise(this);
        }


        /// <summary>
        /// Apply engine steering values.
        /// </summary>
        /// <param name="x">Normalized steering value controlling the X axis engine.</param>
        /// <param name="y">Normalized steering value controlling the Y axis engine.</param>
        /// <param name="z">Normalized steering value controlling the Z axis engine.</param>
        protected void ApplyEngineSteering(float x, float y, float z)
        {
            if (RtdacUsbDriver.Read(state) != 0)
                throw new Exception("Failed to read data from RTDAC/USB device");

            state.PwmGenerators[0].Width = (uint)(1023 * Math.Abs(x));
            state.PwmGenerators[0].Direction = (x < 0.0);
            state.PwmGenerators[0].Brake = Math.Abs(x) < EPSILON;
            state.PwmGenerators[1].Width = (uint)(1023 * Math.Abs(y));
            state.PwmGenerators[1].Direction = (y < 0.0);
            state.PwmGenerators[1].Brake = Math.Abs(y) < EPSILON;
            state.PwmGenerators[2].Width = (uint)(1023 * Math.Abs(z));
            state.PwmGenerators[2].Direction = (z < 0.0);
            state.PwmGenerators[2].Brake = Math.Abs(z) < EPSILON;

            if (RtdacUsbDriver.Write(state) != 0)
                throw new Exception("Failed to write data to RTDAC/USB device");
        }
        
        
        /// <summary>
        /// Apply rail limits.
        /// </summary>
        /// <param name="x">Maximum allowed encoder value for the X axis.</param>
        /// <param name="y">Maximum allowed encoder value for the Y axis.</param>
        /// <param name="z">Maximum allowed encoder value for the Z axis.</param>
        /// <param name="autostopX">Flag indicating automatic power-off when exceeding limit on the X axis</param>
        /// <param name="autostopY">Flag indicating automatic power-off when exceeding limit on the Y axis</param>
        /// <param name="autostopZ">Flag indicating automatic power-off when exceeding limit on the Z axis</param>
        protected void ApplyLimits(float x, float y, float z,
                                   bool autostopX, bool autostopY, bool autostopZ)
        {
        	state.Limits[0].Value = (uint)(x / callibration.XScale / 64.0f);
        	state.Limits[1].Value = (uint)(y / callibration.YScale / 64.0f);
        	state.Limits[2].Value = (uint)(z / callibration.ZScale / 64.0f);
        	state.Limits[0].AutoStop = autostopX;
        	state.Limits[1].AutoStop = autostopY;
        	state.Limits[2].AutoStop = autostopZ;
        	
            if (RtdacUsbDriver.Write(state) != 0)
                throw new Exception("Failed to write data to RTDAC/USB device");
        }
	}
}
