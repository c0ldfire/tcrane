﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace TowerCrane
{
    #region Publicly-visible embedded classes
    /// <summary>
    /// Structure with scaling factors for each encoder.
    /// </summary>
    public class EncoderCallibration
    {
        /// <summary>X axis encoder scale coefficient.</summary>
        public float XScale { get; set; }

        /// <summary>Y angle encoder scale coefficient.</summary>
        public float YScale { get; set; }

        /// <summary>Z axis encoder scale coefficient.</summary>
        public float ZScale { get; set; }

        /// <summary>Alpha angle scale coefficient.</summary>
        public float AlphaScale { get; set; }

        /// <summary>Beta angle scale coefficient.</summary>
        public float BetaScale { get; set; }

        public EncoderCallibration()
        {
            // Default values were extracted from PAR file
            XScale = 0.0000581573f;
            YScale = 0.00157f;
            ZScale = 0.000099484f;
            AlphaScale = 0.001534f;
            BetaScale = 0.001534f;
        }
    }
    /// <summary>
    /// Structure with rail limits and flags.
    /// </summary>
    public class RailLimits
    {
        /// <summary>X axis rail scale coefficient.</summary>
        public float XScale { get; set; }

        /// <summary>Y angle rail scale coefficient.</summary>
        public float YScale { get; set; }

        /// <summary>Z axis rail scale coefficient.</summary>
        public float ZScale { get; set; }

        /// <summary>X axis rail scale flag.</summary>
        public bool XFlag { get; set; }

        /// <summary>Y axis rail scale flag.</summary>
        public bool YFlag { get; set; }

        /// <summary>Z axis rail scale flag.</summary>
        public bool ZFlag { get; set; }
    }
    /// <summary>
    /// Structure with therm flags.
    /// </summary>
    public class ThermFlags
    {
        /// <summary>X axis therm flag.</summary>
        public bool XFlag { get; set; }

        /// <summary>Y axis therm flag.</summary>
        public bool YFlag { get; set; }

        /// <summary>Z axis therm flag.</summary>
        public bool ZFlag { get; set; }
    }
    #endregion

    [XmlRootAttribute()]
    public class Configuration
    {
        #region Tower Crane related settings
        public EncoderCallibration Callibration = new EncoderCallibration();
        public RailLimits RailLimits = new RailLimits();
        public ThermFlags ThermFlags = new ThermFlags();
        public bool AutoReset;
        #endregion


        public void Save(string filename)
        {
            var serializer = new XmlSerializer(typeof(Configuration));
            using (TextWriter writer = new StreamWriter(filename))
                serializer.Serialize(writer, this);
        }

        public static Configuration Load(string filename)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(Configuration));
                using (TextReader reader = new StreamReader(filename))
                    return (Configuration)serializer.Deserialize(reader);
            }
            catch (Exception)
            {
                return new Configuration();
            }
        }
    }
}
