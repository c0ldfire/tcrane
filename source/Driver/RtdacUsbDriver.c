/*
 * RTDAC USB library v0.10
 * Copyright (c) 2015 Mariusz Pasi�ski. All rights reserved.
 *
 * Following RTDAC-USB driver implementation has been written
 * from scratch based on the results of reverse engineering,
 * including online debugging, disassembly and memory analysis.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "RtdacUsbDriver.h"


#define RTDAC_PACKET_SIZE	150
#define RTDAC_CMD_INIT		0x80
#define RTDAC_CMD_WRITE		0x81
#define RTDAC_CMD_READ		0x82

#define HAS_FAILED(FT_cmd)	((s_last_error = FT_cmd) != FT_OK)

#define FAKE_DRIVER 


static uint8_t s_send_buffer[RTDAC_PACKET_SIZE];
static uint8_t s_recv_buffer[RTDAC_PACKET_SIZE];
static uint32_t s_last_error;
static FT_HANDLE s_device;



// --------------------------------------------------------------------
//                          INTERNAL FUNCTIONS
// --------------------------------------------------------------------

///
/// Encode the RT-DAC state to device's native packet.
///
static void SerializeState(const rtdac_state_t *state, uint8_t *packet)
{
	memset(packet, 0, RTDAC_PACKET_SIZE);
	packet[21] = state->pwm_prescaler & 0x3F;
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i) {
		const size_t base = 2 * i + 22;
		packet[base+0] = state->pwm[i].width & 0x7F;			// Low 7-bits
		packet[base+1] = (state->pwm[i].width >> 7) & 0x07;		// High 3-bits
	}
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i)
		packet[28] |= state->pwm[i].direction << i;
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i)
		packet[29] |= state->pwm[i].brake << i;
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i)
		packet[31] |= state->pwm[i].therm_flag << i;

	for (size_t i = 0; i < RTDAC_NUM_LIMITS; ++i) {
		const size_t base = 2 * i + 34;
		packet[base+0] = state->limits[i].limit & 0x7F;			// Low 7-bits
		packet[base+1] = (state->limits[i].limit >> 7) & 0x7F;	// High 7-bits
	}
	for (size_t i = 0; i < RTDAC_NUM_LIMITS; ++i)
		packet[40] |= state->limits[i].flag << i;

	for (size_t i = 0; i < RTDAC_NUM_ENCODERS; ++i)
		packet[41] |= state->encoders[i].reset << i;
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i)
		packet[42] |= state->pwm[i].auto_reset << i;
}


///
/// Decode the device's native packet to RT-DAC state structure.
///
static void DeserializeState(rtdac_state_t *state, const uint8_t *packet)
{
	memset(state, 0, sizeof(rtdac_state_t));
	memcpy(&state->logic_version, packet, 2);
	for (size_t i = 0; i < 5; ++i)
		state->app_name[i] = packet[7 - i];

	state->pwm_prescaler = packet[21];
	for (size_t i = 0; i < RTDAC_NUM_PWMS; ++i) {
		state->pwm[i].width = ((uint32_t)packet[23+2*i] << 8) | packet[22+2*i];
		state->pwm[i].direction = (packet[28] >> i) & 1;
		state->pwm[i].brake = (packet[29] >> i) & 1;
		state->pwm[i].therm = (packet[30] >> i) & 1;
		state->pwm[i].therm_flag = (packet[31] >> i) & 1;
	}

	for (size_t i = 0; i < RTDAC_NUM_LIMITS; ++i) {
		state->limits[i].active = (packet[33] >> i) & 1;
		state->limits[i].limit = (packet[35+2*i] << 8) | packet[34+2*i];
		state->limits[i].flag = (packet[40] >> i) & 1;
	}

	for (size_t i = 0; i < RTDAC_NUM_ENCODERS; ++i) {
		const size_t base = 4 * i + 44;
		state->encoders[i].reset = (packet[41] >> i) & 1;
		state->encoders[i].count = (packet[base+2] << 16)
			                     | (packet[base+1] << 8)
								 | packet[base];
		if (state->encoders[i].count & 0x80000)
			state->encoders[i].count -= 0x100000;
	}

	for (size_t i = 0; i < RTDAC_NUM_COUNTERS; ++i) {
		const size_t base = 4 * i + 66;
		state->counters[i].reset = (packet[64] >> i) & 1;
		state->counters[i].is_timer = (packet[65] >> i) & 1;
		state->counters[i].count = (packet[base+2] << 16)
								 | (packet[base+1] << 8)
								 | packet[base];
	}
}


///
/// Send given buffer to RT-DAC device.
///
static int RtdacIoWrite(uint8_t *buffer, size_t num_bytes)
{
#ifndef FAKE_DRIVER
	DWORD written = 0;
	return s_last_error = FT_Write(s_device, buffer, num_bytes, &written);
#else
	return s_last_error = FT_OK;
#endif
}


///
/// Receive data from the RT-DAC device.
///
static int RtdacIoRead(uint8_t *buffer, size_t num_bytes)
{
#ifndef FAKE_DRIVER
	DWORD read = 0;
	return s_last_error = FT_Read(s_device, buffer, num_bytes, &read);
#else
	return s_last_error = FT_OK;
#endif
}



// --------------------------------------------------------------------
//                           PUBLIC FUNCTIONS
// --------------------------------------------------------------------

int RtdacOpen(void)
{
#ifndef FAKE_DRIVER
	if ( HAS_FAILED(FT_Open(0, &s_device)) || HAS_FAILED(FT_ResetDevice(s_device)) )
		return -1;
	if ( HAS_FAILED(FT_SetLatencyTimer(s_device, 1)) )
		return -2;
	if ( HAS_FAILED(FT_SetUSBParameters(s_device, 0, 0)) )
		return -3;
	if ( HAS_FAILED(FT_SetChars(s_device, 0x3F, true, 0, false)) )
		return -4;

	// Send the 'init' command (this is the only place where the 0x80 is being send)
	s_send_buffer[0] = RTDAC_CMD_INIT;
	RtdacIoWrite(s_send_buffer, RTDAC_PACKET_SIZE);

	DWORD rx_bytes=0, tx_bytes, event;
	if ( HAS_FAILED(FT_GetStatus(s_device, &rx_bytes, &tx_bytes, &event)) )
		return -5;

	// Discard remaining data (XXX: why not using FT_Purge?)
	while (rx_bytes > 0) {
		const size_t bytes_to_read = min(rx_bytes, RTDAC_PACKET_SIZE);
		RtdacIoRead(s_recv_buffer, bytes_to_read);
		rx_bytes -= bytes_to_read;
	}
#endif

	return 0;
}


int RtdacRead(rtdac_state_t *state)
{
	// Send the 'read' command
	s_send_buffer[0] = RTDAC_CMD_READ;
	if (RtdacIoWrite(s_send_buffer, 1) != FT_OK)
		return -5;
	RtdacIoRead(s_recv_buffer, RTDAC_PACKET_SIZE);
	DeserializeState(state, s_recv_buffer);

#ifndef FAKE_DRIVER
	// Discard remaining data (XXX: why not using FT_Purge?)
	DWORD rx_bytes=0, tx_bytes, event;
	FT_GetStatus(s_device, &rx_bytes, &tx_bytes, &event);
	while (rx_bytes > 0) {
		const size_t bytes_to_read = min(rx_bytes, RTDAC_PACKET_SIZE);
		RtdacIoRead(s_recv_buffer, bytes_to_read);
		rx_bytes -= bytes_to_read;
	}
#endif

	return s_last_error;
}


int RtdacWrite(rtdac_state_t *state)
{
	s_send_buffer[0] = RTDAC_CMD_WRITE;
	SerializeState(state, s_send_buffer + 1);
	return RtdacIoWrite(s_send_buffer, RTDAC_PACKET_SIZE);
}


int RtdacClose(void)
{
#ifndef FAKE_DRIVER
	return s_last_error = FT_Close(s_device);
#else
	return s_last_error = FT_OK;
#endif
}