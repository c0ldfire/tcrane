using System;
using System.Runtime.InteropServices;


namespace TowerCrane.Driver
{
    /// <summary>
    /// C# bindings to the reverse-engineered RT-DAC device driver.
    /// </summary>
    internal static class RtdacUsbDriver
    {
        private const string LIBRARY_NAME = "RtdacUsbDriver";


        #region Direct bindings
        /// <summary>
        /// Open connected RT-DAC device.
        /// </summary>
        [DllImport(LIBRARY_NAME, EntryPoint="RtdacOpen", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Open();

        /// <summary>
        /// Close the opened device.
        /// </summary>
        [DllImport(LIBRARY_NAME, EntryPoint="RtdacClose", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Close();

        /// <summary>
        /// Read data from the device.
        /// </summary>
        [DllImport(LIBRARY_NAME, EntryPoint="RtdacRead", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Read(RtdacState state);

        /// <summary>
        /// Write data to the device.
        /// </summary>
        [DllImport(LIBRARY_NAME, EntryPoint="RtdacWrite", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int Write(RtdacState state);
        #endregion
    }
}
