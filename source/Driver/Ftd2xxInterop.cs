﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;


namespace TowerCrane.Driver
{
    /// <summary>
    /// C# bindings to the Future Technology's D2XX device driver.
    /// </summary>
    internal static class Ftd2xx
    {
        private const string LIBRARY_NAME = "ftd2xx";

        /// <summary>
        /// Status codes returned by each function.
        /// </summary>
        public enum Status
        {
            Ok = 0,
            InvalidHandle,
            DeviceNotFound,
            DeviceNotOpened,
            InputOutputError,
            InsufficientResources,
            InvalidParameter,
            InvalidBoudRate,
            DeviceNotOpenedForErase,
            DeviceNotOpenedForWrite,
            FailedToWriteDevice,
            EEPROMReadFailed,
            EEPROMWriteFailed,
            EEPROMEraseFailed,
            EEPROMNotPresent,
            EEPROMNotProgrammed,
            InvalidArguments,
            NotSupported,
            OtherError
        }

        #region Direct bindings
        /// <summary>
        /// Open given device and return a handle which will be used for subsequent accesses.
        /// </summary>
        /// <param name="deviceNumber">Zero-based index of the device to open.</param>
        /// <param name="handle">Pointer to variable where the handle will be stored.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_Open", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_Open(int deviceNumber, out IntPtr handle);

        /// <summary>
        /// Close an open device.
        /// </summary>
        /// <param name="handle">Handle of the device.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_Close", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_Close(IntPtr handle);

        /// <summary>
        /// Read data from the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="buffer">Pointer of the buffer that receives the data from the device.</param>
        /// <param name="bufferSize">Number of bytes to read from the device.</param>
        /// <param name="bytesReturned">Pointer to variable which receives the number of bytes read.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_Read", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_Read(IntPtr handle, IntPtr buffer, uint bufferSize, out uint bytesReturned);

        /// <summary>
        /// Write data to the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="buffer">Pointer to the buffer that contains the data to be written to device.</param>
        /// <param name="bufferSize">Number of bytes to write to the device</param>
        /// <param name="bytesWritten">Pointer to a variable which receives the number of bytes written.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_Write", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_Write(IntPtr handle, IntPtr buffer, uint bufferSize, out uint bytesWritten);

        /// <summary>
        /// Set the read and write timeouts for the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="readTimeout">Read timeout in milliseconds.</param>
        /// <param name="writeTimeout">Write timeout in milliseconds.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_SetTimeouts", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_SetTimeouts(IntPtr handle, uint readTimeout, uint writeTimeout);

        /// <summary>
        /// Gets the device status including current event status and number of bytes in the receive and transmit queue.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="rxBytes">Pointer to a variable which receives the number of bytes in the receive queue.</param>
        /// <param name="txBytes">Pointer to a variable which receives the number of bytes in the transmit queue.</param>
        /// <param name="eventDword">Pointer to a variable which receives the current state of the event status.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_GetStatus", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_GetStatus(IntPtr handle, out uint rxBytes, out uint txBytes, out uint eventDword);

        /// <summary>
        /// Set the special characters for the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="eventChar">Event character.</param>
        /// <param name="eventCharEnabled"></param>
        /// <param name="errorChar">Error character.</param>
        /// <param name="errorCharEnabled"></param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_SetChars", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern uint FT_SetChars(IntPtr handle,
            byte eventChar,
            [param: MarshalAs(UnmanagedType.U1)] bool eventCharEnabled,
            byte errorChar,
            [param: MarshalAs(UnmanagedType.U1)] bool errorCharEnabled);

        /// <summary>
        /// Purge receive and transmit buffers in the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="mask">Combination of FT_PURGE_RX and FT_PURGE_TX.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_Purge", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_Purge(IntPtr handle, uint mask);

        /// <summary>
        /// Send a reset command to the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_ResetDevice", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_ResetDevice(IntPtr handle);

        /// <summary>
        /// Set the latency timer value.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="latency">Required value of latency timer in milliseconds in range [2-255].</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_SetLatencyTimer", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_SetLatencyTimer(IntPtr handle, byte latency);

        /// <summary>
        /// Set the USB request transfer size.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="inTransferSize">Transfer size for USB IN request.</param>
        /// <param name="outTransferSize">Transfer size for USB OUT request.</param>
        /// <returns>Status.Ok if successful</returns>
        [DllImport(LIBRARY_NAME, EntryPoint="FT_SetUSBParameters", CallingConvention=CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Status FT_SetUSBParameters(IntPtr handle, uint inTransferSize, uint outTransferSize);
        #endregion

        #region C# wrappers
        /// <summary>
        /// Read data from the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="buffer">Buffer that receives the data from the device.</param>
        /// <param name="bytesReturned">Pointer to variable which receives the number of bytes read.</param>
        /// <returns>Status.Ok if successful</returns>
        [SecurityPermission(SecurityAction.Demand, UnmanagedCode=true)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static Status FT_Read(IntPtr handle, ref byte[] buffer, out uint bytesReturned)
        {
            GCHandle gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            IntPtr address = gch.AddrOfPinnedObject();
            Status status = FT_Read(handle, address, (uint)buffer.Length, out bytesReturned);
            gch.Free();
            return status;
        }

        /// <summary>
        /// Write data to the device.
        /// </summary>
        /// <param name="handle">Handle of the open device.</param>
        /// <param name="buffer">Buffer that contains the data to be written to device.</param>
        /// <param name="bytesWritten">Pointer to a variable which receives the number of bytes written.</param>
        /// <returns>Status.Ok if successful</returns>
        [SecurityPermission(SecurityAction.Demand, UnmanagedCode=true)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static Status FT_Write(IntPtr handle, byte[] buffer, out uint bytesWritten)
        {
            GCHandle gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            IntPtr address = gch.AddrOfPinnedObject();
            Status status = FT_Write(handle, address, (uint)buffer.Length, out bytesWritten);
            gch.Free();
            return status;
        }
        #endregion
    }
}
