/*
 * RTDAC USB library v0.10
 * Copyright (c) 2015 Mariusz Pasiński. All rights reserved.
 *
 * Following RTDAC-USB driver implementation has been written 
 * from scratch based on the results of reverse engineering,
 * including online debugging, disassembly and memory analysis.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef RTDACUSB_DRIVER_INCLUDED
#define RTDACUSB_DRIVER_INCLUDED

#include <stdint.h>
#include <ftd2xx.h>


#ifdef _WIN32
	#ifdef EXPORT_SYMBOLS
		#define RTDAC_API			__declspec(dllexport)
	#else
		#define RTDAC_API			__declspec(dllimport)
	#endif
#else
	#define RTDAC_API
#endif


#define RTDAC_NUM_PWMS      3	///< Number of PWM wave generators.
#define RTDAC_NUM_LIMITS    3	///< Number of rail limits.
#define RTDAC_NUM_ENCODERS  5	///< Number of incremental encoders.
#define RTDAC_NUM_COUNTERS	1	///< Number of counters/timers.


///
/// Internal RT-DAC state.
///
typedef struct RtdacState
{
    ///
    /// Collection of PWM generator states.
    ///
    struct PwmGenerator {
        uint32_t width;     ///< 10-bit period of the "H" state.
        uint8_t brake;      ///< Brake flag.
        uint8_t direction;  ///< Direction flag.
        uint8_t therm;      ///< ?Amplifires that are overheating? (read-only)
        uint8_t therm_flag; ///< Automatic power down during overheat.
		uint8_t auto_reset; ///< ??
    } pwm[RTDAC_NUM_PWMS];
    
    ///
    /// Collection of encoder states.
    ///
    struct Encoder {
        uint32_t count;     ///< Counter value (read-only).
        uint8_t reset;      ///< Set the count to zero.
    } encoders[RTDAC_NUM_ENCODERS];
    
    ///
    /// Collection of rail limit states.
    ///
    struct RailLimit {
        uint32_t limit;     ///< 14-bit limit.
        uint8_t active;     ///< ? (read-only).
        uint8_t flag;       ///< Stop the PWM when encoder exceedes the limit.
    } limits[RTDAC_NUM_LIMITS];

	///
	/// Collection of counter/timer states.
	///
	struct CounterTimer {
		uint32_t count;		///< ? (read-only)
		uint8_t reset;		///< ? Set the count to zero?
		uint8_t is_timer;	///< ? Make this counter count time.
	} counters[RTDAC_NUM_COUNTERS];
    
    
    uint16_t logic_version; ///< FPGA board logic version (read-only).
    uint8_t  app_name[6];   ///< Application name (read-only).
    uint32_t pwm_prescaler; ///< 6-bit divider for specifing the frequency of PWM waves.
} rtdac_state_t;


RTDAC_API int RtdacOpen(void);
RTDAC_API int RtdacRead(rtdac_state_t *state);
RTDAC_API int RtdacWrite(rtdac_state_t *state);
RTDAC_API int RtdacClose(void);


#endif