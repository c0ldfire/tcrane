﻿using System;
using System.Runtime.InteropServices;


namespace TowerCrane.Driver
{
    /// <summary>
    /// RT-DAC state structure used by the driver.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public class RtdacState
    {
        #region Non-public constants (limits)
        /// <summary>Number of PWM wave generators.</summary>
        protected const int NUM_PWMS = 3;

        /// <summary>Number of rail limits.</summary>
        protected const int NUM_LIMITS = 3;

        /// <summary>Number of measuring encoders.</summary>
        protected const int NUM_ENCODERS = 5;

        /// <summary>Number of counter/timer units.</summary>
        protected const int NUM_COUNTERS = 1;
        #endregion


        #region State structures
        [StructLayout(LayoutKind.Sequential)]
        public struct PwmGenerator
        {
            /// <summary>Gets or sets the 10-bit period of the "H" state.</summary>
            [MarshalAs(UnmanagedType.U4)] public uint Width;

            /// <summary>Gets or sets the brake flag that stops the engine.</summary>
            [MarshalAs(UnmanagedType.U1)] public bool Brake;

            /// <summary>Gets or sets the direction inversion flag.</summary>
            /// <remarks>
            /// Set this flag in order to make the engine go into the negative direction.
            /// </remarks>
            [MarshalAs(UnmanagedType.U1)] public bool Direction;

            /// <summary>Gets the power amplifier's thermal flag.</summary>
            /// <remarks>
            /// This flag is cleared by the device when power amplifier's
            /// temperature is above safe threshold.
            /// </remarks>
            public bool Therm { get { return therm; } }
            [MarshalAs(UnmanagedType.U1)] private bool therm;

            /// <summary>Enables or disables automatic power-off during overheat.</summary>
            [MarshalAs(UnmanagedType.U1)] public bool OverheatPowerDown;

            /// <summary>
            /// Gets or sets the ?
            /// </summary>
            [MarshalAs(UnmanagedType.U1)] public bool AutoReset;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Encoder
        {
            /// <summary>Gets the encoder's value.</summary>
            public uint Value { get { return value; } }
            [MarshalAs(UnmanagedType.U4)] private uint value;

            /// <summary>Enables or disables zeroing the encoder's value.</summary>
            /// <remarks>
            /// Encoder's value will be set zero as long as this flag is set.
            /// The device will not clear it.
            /// </remarks>
            [MarshalAs(UnmanagedType.U1)] public bool Reset;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RailLimit
        {
            /// <summary>Gets or sets the 14-bit rail limit.</summary>
            [MarshalAs(UnmanagedType.U4)] public uint Value;

            /// <summary>Gets the flag determining if encoder's value is in working range.</summary>
            public bool InWorkingRange { get { return inRange; } }
            [MarshalAs(UnmanagedType.U1)] private bool inRange;

            /// <summary>Enable or disable automatic PWM stop during exceeding the limit.</summary>
            [MarshalAs(UnmanagedType.U1)] public bool AutoStop;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CounterTimer
        {
            /// <summary>Gets the counter's value.</summary>
            public uint Value { get { return value; } }
            [MarshalAs(UnmanagedType.U4)] private uint value;

            /// <summary>Enables or disables zeroing the counter's value.</summary>
            [MarshalAs(UnmanagedType.U1)] public bool InWorkingRange;

            /// <summary>Gets or sets the flag determining the counting mode.</summary>
            [MarshalAs(UnmanagedType.U1)] public bool CountTime;
        }
        #endregion


        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = NUM_PWMS)]
        protected PwmGenerator[] pwms = new PwmGenerator[NUM_PWMS];

        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = NUM_ENCODERS)]
        protected Encoder[] encoders = new Encoder[NUM_ENCODERS];

        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = NUM_LIMITS)]
        protected RailLimit[] limits = new RailLimit[NUM_LIMITS];

        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = NUM_COUNTERS)]
        protected CounterTimer[] counters = new CounterTimer[NUM_COUNTERS];

        /// <summary>Gets the FPGA board logic version.</summary>
        public ushort LogicVersion { get { return logicVersion; } }
        [MarshalAs(UnmanagedType.U2)] protected ushort logicVersion;

        /// <summary>Gets the name of the application written to the RTDAC/USB.</summary>
        public string ApplicationName { get { return applicationName; } }
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)] protected string applicationName;

        /// <summary>
        /// Gets or sets the 6-bit divider for specifing the frequency of PWM waves.
        /// </summary>
        [MarshalAs(UnmanagedType.U4)] public uint PwmDivider;

        /// <summary>
        /// Collection of PWM generator states.
        /// </summary>
        public PwmGenerator[] PwmGenerators {
            get { return pwms; }
        }

        /// <summary>
        /// Collection of encoder states.
        /// </summary>
        public Encoder[] Encoders {
            get { return encoders; }
        }

        public RailLimit[] Limits {
            get { return limits; }
        }

        public CounterTimer[] Counters {
            get { return counters; }
        }
    }
}
