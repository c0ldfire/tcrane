﻿using System;
using System.Threading;


namespace TowerCrane
{
    public class CraneController
    {
        protected const float EPSILON = 1e-5f;

        #region Non-public fields
        protected PhysicalTowerCrane crane;
        protected Thread bgThread;
        protected Thread pollThread;
        protected float desiredCartOffset;
        protected float desiredArmAngle;
        protected float desiredLineLength;
        #endregion

        #region Public events
        public event EventHandler DestinationChanged;
        public event EventHandler SteeringChanged;
        #endregion

        #region Direct engine controls (low-level)
        /// <summary>
        /// Gets or sets the cart engine (X axis) steering in [-1.0; 1.0] range.
        /// </summary>
        public float CartEngine {
            get { return crane.XSteering; }
            set {
            	if (bgThread != null) bgThread.Abort();
                crane.XSteering = value;
            }
        }

        /// <summary>
        /// Gets or sets the arm rotation engine (Y axis) steering in [-1.0; 1.0] range.
        /// </summary>
        public float ArmEngine {
            get { return crane.YSteering; }
            set {
            	if (bgThread != null) bgThread.Abort();
                crane.YSteering = value;
            }
        }

        /// <summary>
        /// Gets or sets the lift engine (Z axis) steering in [-1.0; 1.0] range.
        /// </summary>
        public float LiftEngine {
            get { return crane.ZSteering; }
            set {
            	if (bgThread != null) bgThread.Abort();
                crane.ZSteering = value;
            }
        }
        #endregion

        #region High-level control
        /// <summary>
        /// Gets or sets the distance (in meters) of the cart along the X axis.
        /// </summary>
        public float CartPosition {
            get { return crane.XDistance; }
            set {
                desiredCartOffset = value;
                DestinationChanged.Raise(this);
            }
        }

        /// <summary>
        /// Gets or sets the arm angle (in radians?) around the Y axis.
        /// </summary>
        public float ArmAngle {
            get { return crane.YAngle; }
            set {
                desiredArmAngle = value;
                DestinationChanged.Raise(this);
            }
        }

        /// <summary>
        /// Gets or sets the length (in meteres) of the lift-line (Z axis).
        /// </summary>
        public float LiftLineLength {
            get { return crane.ZLength; }
            set { 
                desiredLineLength = value;
                DestinationChanged.Raise(this);
            }
        }
        #endregion

        /// <summary>
        /// Determines whether the crane has reached the desired position/rotation/state.
        /// </summary>
        protected bool HasArrived {
            get {
                return Math.Abs(desiredCartOffset - CartPosition) < EPSILON
                    && Math.Abs(desiredLineLength - LiftLineLength) < EPSILON
                    && Math.Abs(desiredArmAngle - ArmAngle) < EPSILON;
            }
        }


        public PhysicalTowerCrane Crane {
        	get { return crane; }
        }


        public CraneController(PhysicalTowerCrane crane)
        {
            this.crane = crane;
            DestinationChanged += (sender, e) => {
                bgThread = new Thread(NavigateTo);
                bgThread.Start();
            };
            crane.SteeringChanged += (sender, e) => {
                SteeringChanged.Raise(this, e);
            };
        }


        /// <summary>
        /// Stop all kind of crane movements.
        /// </summary>
        public void Stop()
        {
            desiredCartOffset = CartPosition;
            CartEngine = 0.0f;

            desiredArmAngle = ArmAngle;
            ArmEngine = 0.0f;

            desiredLineLength = LiftLineLength;
            LiftEngine = 0.0f;
        }


        private void NavigateTo()
        {
            // The most simple "solver" that uses linear interpolation.
            // NOTE: This function is blocking! Run it in a background thread.
            // NOTE: This implementation does not respect limits!
            const float SPEED = 0.3f;
            const int SLEEP_TIME = 20;

            while (!HasArrived) {
                float xDiff = desiredCartOffset - CartPosition;
                float yDiff = desiredArmAngle - ArmAngle;
                float zDiff = desiredLineLength - LiftLineLength;

                crane.XSteering = (Math.Abs(xDiff) > EPSILON) ? (xDiff < 0.0 ? -1 : 1) * SPEED : 0.0f;
                crane.YSteering = (Math.Abs(yDiff) > EPSILON) ? (yDiff < 0.0 ? -1 : 1) * SPEED : 0.0f;
                crane.ZSteering = (Math.Abs(zDiff) > EPSILON) ? (zDiff < 0.0 ? -1 : 1) * SPEED : 0.0f;
                Thread.Sleep(SLEEP_TIME);
            }
        }
        
        private void CraneUpdater()
        {
        	const int SLEEP_TIME = 50;
        	
        	while (true) {
        		crane.Update();
        		Thread.Sleep(SLEEP_TIME);
        	}
        }
    }
}
