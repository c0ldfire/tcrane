﻿using System;


namespace TowerCrane
{
    public static class SafeInvoker
    {
        /// <summary>
        /// Extension method for raising events in a safe way.
        /// </summary>
        public static void Raise(this EventHandler self, object sender, EventArgs args)
        {
            if (self != null)
                self.Invoke(sender, args);
        }

        /// <summary>
        /// Extension method for raising events in a safe way without event arguments.
        /// </summary>
        public static void Raise(this EventHandler self, object sender)
        {
            self.Raise(sender, EventArgs.Empty);
        }
    }
}
